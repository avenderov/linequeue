# LineQueue

Maintains a queue of text lines and makes it accessible for clients over a TCP connection.
Server is always starting on the same port `10042`. _In the current version it's not possible
to change it._

**This project is just an MVP and intended to show ideas, rather than being used in production.**

## Dependencies

- **Java 11**

## Protocol

- **PUT text**

  Inserts a `text` line into the FIFO Queue. `text` is a single line of 
  an arbitrary number of case-sensitive alphanumeric words ([A-Z]+[a-z]+[0-9]) separated by 
  space characters.
  
- **GET n** 
  
  Return `n` lines from the head of the queue and remove them from the queue.
  If `n` is not a valid line number, the server returns `ERR\r\n`.
  
- **QUIT**

  Client disconnect.
  
- **SHUTDOWN** 
  
  Shutdown the server.
  
## How to build

Execute the script

```shell script
./build.sh
```

Under the hood this script will invoke maven. The project is using meven wrapper, so there is 
no need to have maven being pre-installed on the machine.

All unit and integration tests will be run during the build process.

## How to run

Execute the script without any parameters 

```shell script
./run.sh
```

## Design choices

- As a queue implementation I use [tape](https://square.github.io/tape/). It's a simple 
  persistent queue that has all the capabilities to cover the functional requirements. 
  Data is stored in the `queue.dat` file, that will be created in the directory
  from where the application was started. _In the current version it's not possible to configure
  the file name._ After the restart, if the file already present in the current directory,
  application will use it, instead of creating a new one.
- To simplify implementation of the TCP server I use [vertx](https://vertx.io/). It's a simple library, 
  built on top of Netty, that helps to write asynchronous network applications.
