package com.avenderov.linequeue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Random;

public final class FileTestUtils {

    private static final Logger LOG = LoggerFactory.getLogger(FileTestUtils.class);

    private FileTestUtils() {
    }

    public static String tmpFilename() {
        var random = new Random().nextLong();
        return Long.toUnsignedString(random) + ".tmp";
    }

    public static void deleteRecursively(Path directory) throws IOException {
        Files.walk(directory)
            .sorted(Comparator.reverseOrder())
            .map(Path::toFile)
            .forEach(file -> {
                LOG.debug("Deleting {}", file.getAbsolutePath());
                if (!file.delete()) {
                    LOG.error("Unable to delete {}", file.getAbsolutePath());
                }
            });
    }
}
