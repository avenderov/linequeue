package com.avenderov.linequeue.queue;

import com.avenderov.linequeue.FileTestUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;

class FileLineQueueTest {

    Path tempDirectory;
    FileLineQueue subj;

    @BeforeEach
    void beforeEach() throws IOException {
        tempDirectory = Files.createTempDirectory("tmp");
        subj = new FileLineQueue(new File(tempDirectory.toFile(), FileTestUtils.tmpFilename()));
    }

    @AfterEach
    void afterEach() throws IOException {
        subj.close();
        FileTestUtils.deleteRecursively(tempDirectory);
    }

    @Test
    void shouldEnqueueAndDequeue() {
        // given
        var text = "test";

        // when
        subj.enqueue(text);
        var result = subj.dequeue(1);

        // then
        assertThat(result).containsExactly(text);
    }

    @Test
    void shouldReturnEmptyCollectionIfQueueIsEmpty() {
        // when
        var result = subj.dequeue(1);

        // then
        assertThat(result).isEmpty();
    }

    @Test
    void shouldReturnEmptyIfDequeMoreThanAvailable() {
        // given
        IntStream.range(1, 4).forEach(i -> subj.enqueue("test " + i));

        // when
        var result = subj.dequeue(10);

        // then
        assertThat(result).isEmpty();
    }

    @Test
    void shouldReturnMultipleInOrder() {
        // given
        IntStream.range(1, 4).forEach(i -> subj.enqueue("test " + i));

        // when
        var result = subj.dequeue(2);

        // then
        assertThat(result).containsExactly("test 1", "test 2");
    }

    @Test
    void shouldReturnZeroIfQueueIsEmpty() {
        // when
        var result = subj.size();

        // then
        assertThat(result).isZero();
    }

    @Test
    void shouldReturnSizeOfTheQueue() {
        // given
        IntStream.range(1, 4).forEach(i -> subj.enqueue("test " + i));

        // when
        var result = subj.size();

        // then
        assertThat(result).isEqualTo(3L);
    }

    @Test
    void shouldFinishReadingAfterRestart() throws Exception {
        // given
        var queueFile = new File(tempDirectory.toFile(), FileTestUtils.tmpFilename());
        var firstInstance = new FileLineQueue(queueFile);

        IntStream.range(1, 4).forEach(i -> firstInstance.enqueue("test " + i));

        // when
        var beforeRestart = firstInstance.dequeue(1);
        firstInstance.close();
        var secondInstance = new FileLineQueue(queueFile);
        var afterRestart = secondInstance.dequeue(2);

        // then
        assertThat(beforeRestart).containsExactly("test 1");
        assertThat(afterRestart).containsExactly("test 2", "test 3");
    }

    @Test
    void shouldThrowIfDequeueNegativeAmount() {
        // then
        assertThatIllegalArgumentException()
            .isThrownBy(() -> subj.dequeue(-1))
            .withMessage("value must be greater than 0");
    }
}
