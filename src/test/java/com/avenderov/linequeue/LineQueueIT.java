package com.avenderov.linequeue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.ServerSocket;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

public class LineQueueIT {

    Path tempDirectory;
    Application application;
    TestClient client;

    @BeforeEach
    void beforeEach() throws IOException {
        tempDirectory = Files.createTempDirectory("tmp");
        var port = findAvailablePort();
        application = Application.createApplication(
            new File(tempDirectory.toFile(), FileTestUtils.tmpFilename()), port);
        application.start();
        client = new TestClient(port);
    }

    @AfterEach
    void afterEach() throws Exception {
        client.close();
        application.stop();
        FileTestUtils.deleteRecursively(tempDirectory);
    }

    @Test
    void shouldReturnErrIfQueueIsEmpty() throws Exception {
        // when
        var response = client.exchange("GET 1");

        // then
        assertThat(response).isEqualTo("ERR");
    }

    @Test
    void shouldEnqueueAndDequeueMultiple() throws Exception {
        // when
        client.send("PUT the");
        client.send("PUT quick brown");
        client.send("PUT fox jumps over the");
        client.send("PUT lazy dog");
        var getTwo = client.exchange("GET 2", 2);

        // then
        assertThat(getTwo).containsExactly("the", "quick brown");

        // when
        var getFortyTwo = client.exchange("GET 42");

        // then
        assertThat(getFortyTwo).isEqualTo("ERR");

        // when
        var getOne = client.exchange("GET 1");

        // then
        assertThat(getOne).isEqualTo("fox jumps over the");
    }

    private static int findAvailablePort() {
        try (final var socket = new ServerSocket(0)) {
            return socket.getLocalPort();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
