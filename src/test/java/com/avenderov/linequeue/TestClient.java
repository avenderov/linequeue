package com.avenderov.linequeue;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class TestClient implements Closeable {

    private final Socket socket;
    private final PrintWriter out;
    private final BufferedReader in;

    public TestClient(int port) throws IOException {
        socket = new Socket("127.0.0.1", port);
        out = new PrintWriter(socket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    public void send(String message) {
        out.println(message);
    }

    public String exchange(String message) throws IOException {
        out.println(message);
        return in.readLine();
    }

    public List<String> exchange(String message, int size) throws IOException {
        out.println(message);
        var result = new ArrayList<String>();
        for (int i = 0; i < size; i++) {
            result.add(in.readLine());
        }
        return result;
    }

    @Override
    public void close() throws IOException {
        in.close();
        out.close();
        socket.close();
    }
}
