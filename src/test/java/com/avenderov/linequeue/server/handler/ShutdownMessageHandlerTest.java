package com.avenderov.linequeue.server.handler;

import io.vertx.core.Vertx;
import io.vertx.core.net.NetSocket;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

class ShutdownMessageHandlerTest {

    Vertx vertx = mock(Vertx.class);
    NetSocket socket = mock(NetSocket.class);

    ShutdownMessageHandler subj = new ShutdownMessageHandler(vertx);

    @Test
    void shouldReturnTrueIfCanHandleMessage() {
        // then
        assertThat(subj.canHandle("SHUTDOWN")).isTrue();
    }

    @ParameterizedTest
    @MethodSource("invalidMessages")
    void shouldReturnFalseIfCanNotHandleMessage(String message) {
        // then
        assertThat(subj.canHandle(message)).isFalse();
    }

    @Test
    void shouldCloseVertx() {
        // given
        var message = "SHUTDOWN";

        // when
        subj.handle(socket, message);

        // then
        then(vertx).should().close(any());
    }

    private static Stream<String> invalidMessages() {
        return Stream.of(
            null,
            " SHUTDOWN",
            "SHUTDOWN ",
            "shutdown"
        );
    }
}
