package com.avenderov.linequeue.server.handler;

import com.avenderov.linequeue.queue.LineQueue;
import io.vertx.core.Vertx;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Random;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

class PutMessageHandlerTest {

    Vertx vertx = mock(Vertx.class);
    LineQueue queue = mock(LineQueue.class);

    PutMessageHandler subj = new PutMessageHandler(vertx, queue);

    @Test
    void shouldReturnTrueIfCanHandleMessage() {
        // given
        var message =
            String.format("PUT %s  %s %s ", randomAlphaNumeric(5), randomAlphaNumeric(7), randomAlphaNumeric(9));

        // then
        assertThat(subj.canHandle(message)).isTrue();
    }

    @ParameterizedTest
    @MethodSource("invalidMessages")
    void shouldReturnFalseIfCanNotHandleMessage(String message) {
        // then
        assertThat(subj.canHandle(message)).isFalse();
    }

    private static Stream<String> invalidMessages() {
        return Stream.of(
            null,
            " PUT 1",
            "PUT abc!",
            "put 1",
            "PUT  1"
        );
    }

    private static String randomAlphaNumeric(int size) {
        var alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        var random = new Random();
        var builder = new StringBuilder(size);
        for (int i = 0; i < size; i++) {
            builder.append(alphabet.charAt(random.nextInt(alphabet.length())));
        }
        return builder.toString();
    }
}
