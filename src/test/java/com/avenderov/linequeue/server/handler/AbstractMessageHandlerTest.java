package com.avenderov.linequeue.server.handler;

import io.vertx.core.Vertx;
import io.vertx.core.net.NetSocket;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThatIllegalStateException;
import static org.mockito.Mockito.mock;

class AbstractMessageHandlerTest {

    Vertx vertx = mock(Vertx.class);
    ThrowingMessageHandler subj = new ThrowingMessageHandler(vertx);

    @Test
    void shouldThrowIfCanNotHandleMessage() {
        // given
        var socket = mock(NetSocket.class);
        var message = UUID.randomUUID().toString();

        // then
        assertThatIllegalStateException()
            .isThrownBy(() -> subj.handle(socket, message))
            .withMessageContainingAll(subj.getClass().getSimpleName(), "can't handle message", message);
    }

    public static class ThrowingMessageHandler extends AbstractMessageHandler {

        public ThrowingMessageHandler(Vertx vertx) {
            super(vertx);
        }

        @Override
        protected void handleValidMessage(NetSocket socket, String message) {
            throw new UnsupportedOperationException("should not be called");
        }

        @Override
        public boolean canHandle(String message) {
            return false;
        }
    }
}
