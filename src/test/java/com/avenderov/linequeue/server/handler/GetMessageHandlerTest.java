package com.avenderov.linequeue.server.handler;

import com.avenderov.linequeue.queue.LineQueue;
import io.vertx.core.Vertx;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Random;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

class GetMessageHandlerTest {

    Vertx vertx = mock(Vertx.class);
    LineQueue queue = mock(LineQueue.class);

    GetMessageHandler subj = new GetMessageHandler(vertx, queue);

    @Test
    void shouldReturnTrueIfCanHandleMessage() {
        // given
        var random = new Random();
        var count = random.nextInt(Integer.MAX_VALUE - 1) + 1;
        var message = String.format("GET %s", count);

        // then
        assertThat(subj.canHandle(message)).isTrue();
    }

    @ParameterizedTest
    @MethodSource("invalidMessages")
    void shouldReturnFalseIfCanNotHandleMessage(String message) {
        // then
        assertThat(subj.canHandle(message)).isFalse();
    }

    private static Stream<String> invalidMessages() {
        return Stream.of(
            null,
            " GET 1",
            "GET -1",
            "GET 0",
            "get 1",
            "GET  1"
        );
    }
}
