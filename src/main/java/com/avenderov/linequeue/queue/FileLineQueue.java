package com.avenderov.linequeue.queue;

import com.avenderov.linequeue.validation.Checks;
import com.squareup.tape2.ObjectQueue;
import com.squareup.tape2.QueueFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.locks.ReentrantLock;

/**
 * File based queue that is backed by <a href="https://square.github.io/tape/">tape</a>.
 */
public final class FileLineQueue implements LineQueue, Closeable {

    private static final Logger LOG = LoggerFactory.getLogger(FileLineQueue.class);

    private final ReentrantLock lock = new ReentrantLock(true);
    private final ObjectQueue<String> queue;

    public FileLineQueue(File queueFile) {
        this(buildQueue(queueFile));
    }

    FileLineQueue(ObjectQueue<String> queue) {
        this.queue = Objects.requireNonNull(queue);
    }

    private static ObjectQueue<String> buildQueue(File queueFile) {
        Objects.requireNonNull(queueFile);
        try {
            return ObjectQueue.create(new QueueFile.Builder(queueFile).build(), StringConverter.INSTANCE);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public void enqueue(String text) {
        Checks.checkNotBlank(text);
        lock.lock();
        try {
            LOG.trace("Enqueueing value: {}", text);
            queue.add(text);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Collection<String> dequeue(int count) {
        Checks.checkGreaterThanZero(count);
        lock.lock();
        try {
            LOG.trace("De-queueing {} value(s)", count);
            if (count > queue.size()) {
                return Collections.emptyList();
            }

            final var result = queue.peek(count);
            queue.remove(count);

            return result;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public int size() {
        lock.lock();
        try {
            return queue.size();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void close() throws IOException {
        final var queueFile = queue.file();
        if (queueFile != null) {
            queueFile.close();
        }
    }

    private static class StringConverter implements ObjectQueue.Converter<String> {

        public static final StringConverter INSTANCE = new StringConverter();

        @Override
        public String from(byte[] bytes) {
            return new String(bytes, StandardCharsets.UTF_8);
        }

        @Override
        public void toStream(String value, OutputStream os) throws IOException {
            os.write(value.getBytes(StandardCharsets.UTF_8));
        }
    }
}
