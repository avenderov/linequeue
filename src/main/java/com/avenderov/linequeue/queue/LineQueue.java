package com.avenderov.linequeue.queue;

import java.util.Collection;

public interface LineQueue {

    /**
     * Inserts a text line into the FIFO queue.
     *
     * @param text text to put into the queue
     */
    void enqueue(String text);

    /**
     * Returns n lines from the head of the queue and remove them from the queue.
     *
     * @param count number of lines to return
     * @return first {@code count} lines, or empty collection if queue doesn't have enough elements
     */
    Collection<String> dequeue(int count);

    /**
     * Returns the size of the queue.
     *
     * @return size of the queue, or {@code 0} if queue is empty
     */
    int size();
}
