package com.avenderov.linequeue.validation;

public final class Checks {

    private Checks() {
    }

    public static String checkNotBlank(String value) {
        if (value == null || value.isBlank()) {
            throw new IllegalArgumentException("value must not be blank");
        }
        return value;
    }

    public static int checkGreaterThanZero(int value) {
        if (value <= 0) {
            throw new IllegalArgumentException("value must be greater than 0");
        }
        return value;
    }

    public static void checkState(boolean condition, String message) {
        if (!condition) {
            throw new IllegalStateException(message);
        }
    }
}
