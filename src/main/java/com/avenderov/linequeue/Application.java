package com.avenderov.linequeue;

import com.avenderov.linequeue.queue.FileLineQueue;
import com.avenderov.linequeue.server.ConnectHandler;
import com.avenderov.linequeue.server.TcpServer;
import com.avenderov.linequeue.server.handler.GetMessageHandler;
import com.avenderov.linequeue.server.handler.PutMessageHandler;
import com.avenderov.linequeue.server.handler.QuitMessageHandler;
import com.avenderov.linequeue.server.handler.ShutdownMessageHandler;
import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Application entrypoint.
 */
public final class Application {

    private static final Logger LOG = LoggerFactory.getLogger(Application.class);

    private static final int DEFAULT_SERVER_PORT = 10042;

    private final AtomicBoolean started = new AtomicBoolean(false);
    private final Vertx vertx;
    private final TcpServer tcpServer;

    private Application(Vertx vertx, TcpServer tcpServer) {
        this.vertx = Objects.requireNonNull(vertx);
        this.tcpServer = Objects.requireNonNull(tcpServer);
    }

    public static Application createApplication(File queueFile) {
        return createApplication(queueFile, DEFAULT_SERVER_PORT);
    }

    public static Application createApplication(File queueFile, int port) {
        final var vertx = Vertx.vertx();

        final var queue = new FileLineQueue(queueFile);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                queue.close();
                LOG.info("Queue closed successfully");
            } catch (IOException e) {
                LOG.error("Exception while closing the queue", e);
            }
        }));
        final var connectHandler =
            new ConnectHandler(
                List.of(
                    new PutMessageHandler(vertx, queue),
                    new GetMessageHandler(vertx, queue),
                    new QuitMessageHandler(vertx),
                    new ShutdownMessageHandler(vertx)
                )
            );
        final var tcpServer = new TcpServer(port, connectHandler);

        return new Application(vertx, tcpServer);
    }

    public static void main(String[] args) {
        createApplication(new File("queue.dat")).start();
    }

    public synchronized void start() {
        if (started.get()) {
            throw new IllegalStateException("Application already started");
        }

        LOG.info("Starting application");
        final var deployLatch = new CountDownLatch(1);
        vertx.deployVerticle(tcpServer, result -> {
            if (result.succeeded()) {
                deployLatch.countDown();
            } else {
                throw new IllegalStateException("Exception while starting the server", result.cause());
            }
        });
        try {
            deployLatch.await();
        } catch (InterruptedException e) {
            throw new IllegalStateException("Startup was interrupted", e);
        }
        started.set(true);
    }

    public synchronized void stop() {
        if (!started.get()) {
            throw new IllegalStateException("Application is not started");
        }
        final var closeLatch = new CountDownLatch(1);
        vertx.close(result -> {
            if (result.succeeded()) {
                closeLatch.countDown();
            } else {
                LOG.error("Exception while stopping the server", result.cause());
            }
        });
        try {
            closeLatch.await();
        } catch (InterruptedException e) {
            LOG.error("Shutdown was interrupted", e);
        }
    }
}
