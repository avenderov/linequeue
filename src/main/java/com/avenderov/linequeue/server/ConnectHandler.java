package com.avenderov.linequeue.server;

import com.avenderov.linequeue.server.handler.MessageHandler;
import io.vertx.core.Handler;
import io.vertx.core.net.NetSocket;
import io.vertx.core.parsetools.RecordParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Objects;

/**
 * Handles connection of a new client.
 */
public final class ConnectHandler implements Handler<NetSocket> {

    private static final Logger LOG = LoggerFactory.getLogger(ConnectHandler.class);

    private static final String NEW_LINE = "\n";

    private final Collection<MessageHandler> handlers;

    public ConnectHandler(Collection<MessageHandler> handlers) {
        this.handlers = Objects.requireNonNull(handlers);
    }

    @Override
    public void handle(NetSocket socket) {
        final var clientId = System.identityHashCode(socket);
        LOG.info("New client connected: {}", clientId);

        socket
            .closeHandler(__ ->
                LOG.info("Client {} disconnected", clientId))
            .handler(RecordParser.newDelimited(NEW_LINE, buffer -> {
                final var message = buffer.toString();
                LOG.debug("Received message from client {}: {}", clientId, message);

                // route message to an appropriate handler
                handlers
                    .stream()
                    .filter(handler -> handler.canHandle(message))
                    .findFirst()
                    .ifPresentOrElse(
                        handler -> handler.handle(socket, message),
                        () -> LOG.warn("Unsupported message: {}", message)
                    );
            }));
    }
}
