package com.avenderov.linequeue.server.handler;

import io.vertx.core.Vertx;
import io.vertx.core.net.NetSocket;

/**
 * Defines a contract for various protocol message handlers.
 */
public interface MessageHandler {

    Vertx getVertx();

    /**
     * Checks if the message can be handled by this handler.
     *
     * @param message incoming message
     * @return {@code true} if this handler is capable of handling this message, {@code false} otherwise
     */
    boolean canHandle(String message);

    /**
     * Message handling logic.
     *
     * @param socket  connection to be used to send back response
     * @param message incoming message
     */
    void handle(NetSocket socket, String message);
}
