package com.avenderov.linequeue.server.handler;

import io.vertx.core.Vertx;
import io.vertx.core.net.NetSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handles {@code SHUTDOWN} message.
 */
public final class ShutdownMessageHandler extends AbstractMessageHandler {

    private static final Logger LOG = LoggerFactory.getLogger(ShutdownMessageHandler.class);

    private static final String COMMAND = "SHUTDOWN";

    public ShutdownMessageHandler(Vertx vertx) {
        super(vertx);
    }

    @Override
    public boolean canHandle(String message) {
        return COMMAND.equals(message);
    }

    @Override
    protected void handleValidMessage(NetSocket socket, String message) {
        LOG.info("Shutting down the server...");
        getVertx().close(result -> {
            if (result.succeeded()) {
                LOG.info("Server shutdown");
            } else {
                LOG.error("Exception while shutting down", result.cause());
            }
        });
    }
}
