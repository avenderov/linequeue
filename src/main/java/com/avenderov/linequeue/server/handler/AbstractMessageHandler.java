package com.avenderov.linequeue.server.handler;

import com.avenderov.linequeue.validation.Checks;
import io.vertx.core.Vertx;
import io.vertx.core.net.NetSocket;

import java.util.Objects;

abstract class AbstractMessageHandler implements MessageHandler {

    private final Vertx vertx;

    protected AbstractMessageHandler(Vertx vertx) {
        this.vertx = Objects.requireNonNull(vertx);
    }

    @Override
    public final Vertx getVertx() {
        return vertx;
    }

    /**
     * Ensures that message can be processed by the current handler.
     * After check delegates to {@link #handleValidMessage(NetSocket, String)}.
     */
    @Override
    public final void handle(NetSocket socket, String message) {
        Checks.checkState(canHandle(message),
            String.format("Handler %s can't handle message: %s", handlerName(), message));
        handleValidMessage(socket, message);
    }

    private String handlerName() {
        return this.getClass().getSimpleName();
    }

    protected abstract void handleValidMessage(NetSocket socket, String message);
}
