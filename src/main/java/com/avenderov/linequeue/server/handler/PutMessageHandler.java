package com.avenderov.linequeue.server.handler;

import com.avenderov.linequeue.queue.LineQueue;
import com.avenderov.linequeue.util.Promises;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.net.NetSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Handles {@code PUT text} message.
 */
public final class PutMessageHandler extends AbstractMessageHandler {

    private static final Logger LOG = LoggerFactory.getLogger(PutMessageHandler.class);

    private static final String COMMAND = "PUT";
    private static final Pattern PUT_MESSAGE = Pattern.compile(String.format("^%s ([a-zA-Z0-9]+\\s*)+$", COMMAND));
    private static final int TEXT_START_INDEX = COMMAND.length() + 1; // command + space

    private final LineQueue queue;

    public PutMessageHandler(Vertx vertx, LineQueue queue) {
        super(vertx);
        this.queue = Objects.requireNonNull(queue);
    }

    @Override
    public boolean canHandle(String message) {
        return message != null && PUT_MESSAGE.matcher(message).matches();
    }

    @Override
    protected void handleValidMessage(NetSocket socket, String message) {
        getVertx().executeBlocking(enqueue(message), this::handleResult);
    }

    private Handler<Promise<String>> enqueue(String message) {
        return promise ->
            Promises.tryCompleteWithResult(promise, () -> {
                final var text = message.substring(TEXT_START_INDEX).trim();
                queue.enqueue(text);
                return text;
            });
    }

    private void handleResult(AsyncResult<String> result) {
        if (result.succeeded()) {
            LOG.debug("Successfully enqueued: {}", result.result());
        } else {
            LOG.error("Exception while enqueueing", result.cause());
        }
    }
}
