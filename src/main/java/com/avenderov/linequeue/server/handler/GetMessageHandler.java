package com.avenderov.linequeue.server.handler;

import com.avenderov.linequeue.queue.LineQueue;
import com.avenderov.linequeue.util.Promises;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.net.NetSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Handles {@code GET n} message.
 */
public final class GetMessageHandler extends AbstractMessageHandler {

    private static final Logger LOG = LoggerFactory.getLogger(GetMessageHandler.class);

    private static final String COMMAND = "GET";
    private static final Pattern GET_MESSAGE = Pattern.compile(String.format("^%s [1-9]\\d*$", COMMAND));
    private static final int COUNT_START_INDEX = COMMAND.length() + 1; // command + space

    private static final String ERROR = "ERR\r\n";
    private static final String NEW_LINE = "\n";

    private final LineQueue queue;

    public GetMessageHandler(Vertx vertx, LineQueue queue) {
        super(vertx);
        this.queue = Objects.requireNonNull(queue);
    }

    @Override
    public boolean canHandle(String message) {
        return message != null && GET_MESSAGE.matcher(message).matches();
    }

    @Override
    protected void handleValidMessage(NetSocket socket, String message) {
        getVertx().executeBlocking(dequeue(message), handleResult(socket));
    }

    private Handler<Promise<Collection<String>>> dequeue(String message) {
        return promise ->
            Promises.tryCompleteWithResult(promise, () -> {
                final var count = Integer.parseUnsignedInt(message.substring(COUNT_START_INDEX));
                return queue.dequeue(count);
            });
    }

    private Handler<AsyncResult<Collection<String>>> handleResult(NetSocket socket) {
        return result -> {
            if (result.succeeded()) {
                if (result.result().isEmpty()) {
                    socket.write(ERROR);
                } else {
                    LOG.debug("Successfully de-queued {} items", result.result().size());
                    result.result().forEach(text -> socket.write(text + NEW_LINE));
                }
            } else {
                LOG.error("Exception while de-queueing", result.cause());
            }
        };
    }
}
