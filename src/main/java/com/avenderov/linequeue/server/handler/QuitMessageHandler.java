package com.avenderov.linequeue.server.handler;

import io.vertx.core.Vertx;
import io.vertx.core.net.NetSocket;

/**
 * Handles {@code QUIT} message.
 */
public final class QuitMessageHandler extends AbstractMessageHandler {

    private static final String COMMAND = "QUIT";

    public QuitMessageHandler(Vertx vertx) {
        super(vertx);
    }

    @Override
    public boolean canHandle(String message) {
        return COMMAND.equals(message);
    }

    @Override
    protected void handleValidMessage(NetSocket socket, String message) {
        socket.close();
    }
}
