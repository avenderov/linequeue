package com.avenderov.linequeue.server;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.net.NetSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

import static com.avenderov.linequeue.validation.Checks.checkGreaterThanZero;

public final class TcpServer extends AbstractVerticle {

    private static final Logger LOG = LoggerFactory.getLogger(TcpServer.class);

    private final int port;
    private final Handler<NetSocket> connectHandler;

    public TcpServer(int port, Handler<NetSocket> connectHandler) {
        this.port = checkGreaterThanZero(port);
        this.connectHandler = Objects.requireNonNull(connectHandler);
    }

    @Override
    public void start(Promise<Void> start) {
        getVertx()
            .createNetServer()
            .connectHandler(connectHandler)
            .listen(port, result -> {
                if (result.succeeded()) {
                    LOG.info("Server started on port {}", result.result().actualPort());
                    start.complete();
                } else {
                    start.fail(result.cause());
                }
            });
    }
}
