package com.avenderov.linequeue.util;

import io.vertx.core.Promise;

import java.util.Objects;
import java.util.function.Supplier;

public final class Promises {

    private Promises() {
    }

    public static <T> void tryCompleteWithResult(Promise<T> promise, Supplier<T> executable) {
        Objects.requireNonNull(promise);
        Objects.requireNonNull(executable);
        try {
            promise.complete(executable.get());
        } catch (RuntimeException e) {
            promise.fail(e);
        }
    }
}
