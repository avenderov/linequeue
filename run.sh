#!/usr/bin/env sh

EXEC=./target/linequeue-jar-with-dependencies.jar
if [ ! -f "$EXEC" ]; then
    echo "Executable not found. Please run ./build.sh first."
    exit 1
fi

java -jar ./target/linequeue-jar-with-dependencies.jar
